package com.dreadedcheez.tntbgone;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid= TnTbGone.MOD_ID, name= TnTbGone.MOD_NAME, version= TnTbGone.MOD_VERSION)
public class TnTbGone {
	
	public static final String MOD_ID = "tntbgone";
	public static final String MOD_NAME = "TnTbGone";
    public static final String MOD_VERSION = "1.0";
    public static final String MOD_MCVERSION = "1.7.2";
    public static final String MOD_AUTHOR = "DreadedCheez";
    
    // The instance of your mod that Forge uses.
    @Instance(value = MOD_ID)
    public static TnTbGone instance;
   
    // Says where the client and server 'proxy' code is loaded.
    @SidedProxy(clientSide="com.dreadedcheez.tntbgone.client.ClientProxy", serverSide="com.dreadedcheez.tntbgone.CommonProxy")
    public static CommonProxy proxy;
   
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
            // Stub Method
    }
   
    @SuppressWarnings("static-access")
	@EventHandler
    public void load(FMLInitializationEvent event)
    {
            proxy.RemoveTNT(new ItemStack(Blocks.tnt));
    }
   
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
            // Stub Method
    }
}